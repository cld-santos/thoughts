title: Broken window theory
summary: Broken window principle applied to the experience of washing dishes and how it can improve your bug solving spirit.

# Broken window theory

Washing dishes has been always an unpleasant task until I began finding some similarities between my past experiences while trying to cultivate a proactive solving bug spirit among my co-workers. Since that time, I started to write down some thoughts about this concept and that's what I want to share with you through this post.

A dirty dish is something produced as a side effect of the main task of cooking. If you replace 'dirty dish' for 'bug' and 'cooking' for 'coding', you'll end up being a better person, jokes aside what I want to highlight is the similarity on both tasks and its side effects. There is no way to avoid it, there are indeed ways to reduce it, but that's not our scope here, so I would like to introduce you some practices that reduce the heavy of this burden that could guide you to handle it in a lightweight way.

## Make peace with yourself

The most important thing I've learned from washing dishes is that there is no guilt, people were filled with goodwill when they were writing code, no one wake up and plan to create bugs, so don't worry you're not a bad person if you end up creating bugs and for sure you're not less smart if you do so. Let's make peace with ourselves and face it as an adult, there is not only one Chef who suffers from dirty dishes. Be proud of what you can create and don't be ashamed of the dishes you have dirt on the way.

## It will not get fixed by itself

It's ok to produce it, but once you discover it, don't let it there, fix it! You should only be ashamed by the bugs that you let alive to someone else catch and fix for you.

## The broken window theory

There is another side effect from left bugs around. Disorder creates disorder and if no one cares about it the question that will come to your co-workers is "why should I care tough?" and that is the thinking when we face a system filled up with bugs and also this is a theory observed and proved by many researchers as you can get details [here](https://en.wikipedia.org/wiki/Broken_windows_theory).
After reading it, I decided to conduct an experiment myself, it's worth to note that this experiment is not related to my laziness xD. So, to prove the theory when the sink is clean everyone will be inclined to keep it as it is, otherwise, if I have some dirty dishes it will get worst.
The results I have observed:
 - When the kitchen was clean, no one throw garbage into the sink, no one left things out of place, some times my oldest son washed some dishes and the mood was so better that my wife did cook some special desserts.
 - When the kitchen was dirty, I found a sort of different garbages into the sink, the mood was terrible in a level that some arguings started to happen, no one took the attitude to wash dishes and I end up spending too much time washing and complaining and no congrats was given and also no desserts were made.

## Make it visible

My oldest son got the habit to hide her bedroom mess by moving it away from there to the other rooms. Sometimes after having washed a full sink of dishes some new dirty ones magically appear, then I had the brilliant idea to use the same tactic, something that temporarily fixed my problem but ends up breaking some dishes or increasing the time spending on the wonderful task to have all dishes cleaned. So just move things can be harmful and inefficient as well.
That is the same thing that happens when we decide to cover some bugs, no one will work and prioritize that and you'll found this bug at an inconvenient time.

## Take ownership

That's when this process gets really interesting. Just by having all of these beautiful concepts on how to deal with the dishes will not help you to avoid mental stress. 

By being the unique responsible for the dishes, I was always complaining and arguing about why I was the only one who needed to care about that. My wife had always to ask me to do it and while I was washing the dishes I was complaining of doing it and also she was complaining because she needed to play the villain.

Then I saw a random cartoon that made a joke about this situation and I end up realizing that through a simple switching of mindset by starting facing this task as a life commitment to keep the kitchen clean, instead of a burden frees my mind and brought peace back to my house.


**So finally, I hope** that by seeing this common example that most of us may face during our daily basis routine, can let you applied some small actions on a real problem, like dirty dishes, and may help you to be confident to bring the same actions and concepts into your team's routine.



