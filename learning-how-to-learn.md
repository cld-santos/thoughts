title: Learning how to learn
summary: Have you read the top 3 best sellers and still struggling on how to sell ideas on how to develop and to architect SW for your team, why all of this gathered information doesn't help you at all?


## Learning how to learn

disclaimer: this is not a scientific study, it's just an amateur studying on how to solve a few issues he has been facing as a software developer. A field which requires a huge effort to absorb, keep and use information.

Since this is a wide subject, I'll create few assumptions to help us through the understanding of how we create knowledge to help us in daily routines.

 - Study field: software development teams and programmers;
 - Study object: how to convenient use that information we gather at our daily routine to help us make decisions and discuss topics;

This post came to my mind because a friend of mine complains he has read all top SW Patterns books and this doesn't help him at his daily routine.

Why not?

First let's understand how our memory works, by [Wikipedia](https://en.wikipedia.org/wiki/Memory)

A brief try think on a graph or network, when something makes sense a synapse is created, it will be there, once you never tread that path again it will remain at your deep memory, but if you reach that information often, then it will keep fresh and also more paths will reach that synapse. Until that path keeps been thread again and again and again you end up enforcing that information.

A few months ago a friend of mine realized that he was working on a project that I've created from the ground. We've spent an entire afternoon talking about all the details and concepts of a project that I have worked three years ago, which proves to me that once you got deeply involved for a long time on a project you end up having a lot of easily accessible memories. I think that the reason why it's easily accessible is that I've lived a lot of different experiences while I was coding that software.

Few renowned having been saying and taking advantage of that way of learning:

 - Fabio Akita a renowned ruby developer said at a talk that's is truly simple to learn what you need to learn, just read, practice and keep this practice/study for each week, then for each month, increasing the frequency until this become natural to you.
 - Some researchers have been saying that the learning process includes experimentation and failures to find success. You can see references about it at [Freedom to Learn](https://www.goodreads.com/book/show/220862.Freedom_to_Learn) by Carl R. Rogers, at a TEDx Talk named [How to learn? From mistakes](https://www.ted.com/talks/diana_laufenberg_3_ways_to_teach?language=en&utm_campaign=tedspread&utm_medium=referral&utm_source=tedcomshare) and on [how Spotify celebrate failures](https://hrblog.spotify.com/2017/10/30/why-we-love-failure/).


I've been reaching pretty good results on keeping a notebook of the main subject I have to learn. This notebook must be reviewed every week and also I try to re-write my explanation on a subject. That works on theory, but to learn different kinds of algorithms I have to keep the practice to solve new problems and review the old ones. That helped me to achieve a very good score at my English Course and to become a ThoughtWorker.

I'm giving you reasonable information to ensure that I'll help you to increase your potential to learn new things and to keep that knowledge, but unfortunately that's not enough, you must study subjects that are pleasing to you and which motivate you to reach what you define as a successful life.

