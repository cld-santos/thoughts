title: The perception of wrong.
summary: My analysis on how the perception of wrong is harmful for a software even when you are using the best methodologies and tools to write code.

# The perception of wrong

During the last days I’ve been facing new challenges and practicing a few algorithms using mainly Hackerrank, but not exclusively. At least 3 of those code challenging websites has the same methodology to asses code submissions. They give you a problem and some tests that allow you to check if your code works as you expect, but once your code is submitted the website will run your code against a lot more tests than those provided in the first place and finally you'll realize what were your mistakes. This process caught my attention and I’ve decided to write about it.

So, let’s take a step back and think about how we conceive a solution or at least what are the popular methodologies to do this.

 - TDD: Test driven development, Red, green and refactor. Write a code which fails, fix it, refactor then do it again.
 - SOLID:  Single Responsibility, Open/Close, Liskov, Interface Segregation and Dependency Inversion.

All of this will drive us to develop software well architected, simple to extend and easy to maintain, AWESOME!

It’s not my intention to sentence these famous methodologies of having this conceptual lack. I’m just trying to put some lights at this particular moment on conceiving a module, class or function, saying that to just rely on these methodologies is not enough and this is so evident that we are seeing these websites taking advantage of this.


The reason why they took advantage of that is that we developers have been developing software with a misconception of what is wrong.

So, my point is, we waste too much time thinking to solve design problems and end up evaluating the correctness of our code only based on how easy my class is extensible, but if we think on it again, we are extending garbage.

The most important thing we have been neglecting is the algorithm complexity. We left this subject for those guys who work at unicorn companies, who create libraries and frameworks, then we become a simple developer who just put pieces together without realizing what is going on under the hood.


To master this subject the first step is to watch [this talk](https://youtu.be/duvZ-2UK0fc) given by Ned Batchelder on Big-O: How Code Slows as Data Grows at PyCon 2018 and another [video](https://youtu.be/v4cd1O4zkGw) by Gayle Laakmann McDowell.

Next step, you must understand the common Algorithms and Data Structure. I recommend [this module at Hackerrank.com](https://www.hackerrank.com/interview/interview-preparation-kit), almost all the most important challenges have a short video by Gayle Laakmann McDowell, author of [Cracking the Coding Interview](http://a.co/d/4zmlbCN), explaining the concept and how to solve those challenges. The list below will help you on your studies:

 - [Tips and Guidelines](https://www.hackerrank.com/interview/interview-preparation-kit/tips-and-guidelines/videos)

### Data Structure

 - [Arrays](https://www.hackerrank.com/challenges/ctci-array-left-rotation/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=arrays)
 - [String and String Builders](https://www.hackerrank.com/challenges/ctci-making-anagrams/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=strings)
 - [Hash Tables](https://www.hackerrank.com/challenges/ctci-ransom-note/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps)
 - [Stacks and Queues](https://www.hackerrank.com/challenges/ctci-queue-using-two-stacks/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=stacks-queues)
 - [Tree](https://www.hackerrank.com/challenges/ctci-is-binary-search-tree/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=trees)
 - [Linked List](https://www.hackerrank.com/challenges/ctci-linked-list-cycle/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=linked-lists)
 - [Heaps](https://www.hackerrank.com/challenges/ctci-find-the-running-median/problem)

### Algorithms
 - [Tries](https://www.hackerrank.com/challenges/ctci-contacts/problem)
 - [Recursion](https://www.hackerrank.com/challenges/ctci-fibonacci-numbers/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=recursion-backtracking)
 - [Bubble Sort](https://www.hackerrank.com/challenges/ctci-bubble-sort/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=sorting)
 - [Quick Sort](https://www.hackerrank.com/challenges/ctci-comparator-sorting/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=sorting)
 - [Merge Sort](https://www.hackerrank.com/challenges/ctci-merge-sort/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=sorting)
 - [Binary Search](https://www.hackerrank.com/challenges/ctci-ice-cream-parlor/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=search)
 - [Graph Search](https://www.hackerrank.com/challenges/ctci-bfs-shortest-reach/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=graphs)

My hipotheses is that we as software developers switch our studies from the fundamental concepts of Computer Science to Software Design in such way that those code challenge websites mechanisms show us that we are indeed spending less time studying fundamentals, but unfourtunatelly there are no hidden tests to check if our code will work as expected before move to the wild environment called production. So would be better to embrace it and write those by your own.

I hope you have improved your perception of wrong.
